package com.avenuecode.blockchain;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;

@Slf4j
public class BlockChain {

    private ArrayList<Block> blockchain;

    public BlockChain(ArrayList<Block> blockchain) {
        this.blockchain = blockchain;
        this.blockchain.add(genesisBlock());
        log.info("First block added to the chain with hash = " + genesisBlock().getHash());
    }

    public String getLastHash() {
        return this.blockchain.get(this.blockchain.size() - 1).getHash();
    }

    private Block genesisBlock() {
        Block genesis = new Block("First Block", null);
        genesis.calculateBlockHash();
        return genesis;
    }

    public synchronized void addBlock(Block block, int difficulty, int miner) {
        if (isValid(block, difficulty)) {
            this.blockchain.add(block);
            log.info("Block Added to the chain by the Miner #" + miner + " with hash = " + block.getHash());
        }
    }

    public Boolean isValid(Block block, int difficulty) {
        return block.getHash().equals(block.calculateBlockHash())
                && block.getHash().startsWith(new String(new char[difficulty]).replace('\0', '0'))
                && this.getLastHash().equals(block.getPreviousHash());
    }
}